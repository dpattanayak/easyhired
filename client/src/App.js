import './css/App.css';
import Signup from './components/signup';
import Signin from './components/signin';
import { Route, Switch } from 'react-router-dom';

function App(){
  return (
    <div className="App">
      <Switch>
        <Route path="/signup" component={Signup} />
        <Route path='/' component={Signin} />
      </Switch>
    </div>
  );
}

export default App;
